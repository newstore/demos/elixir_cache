import Config

config :cache_demo, CacheDemo, port: System.get_env("PORT", "9000") |> String.to_integer()

config :libcluster,
  topologies: [
    local: [strategy: Elixir.Cluster.Strategy.LocalEpmd]
  ]
