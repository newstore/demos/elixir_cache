defmodule CacheDemo.Repo do
  use Nebulex.Caching

  @ttl :timer.hours(1)

  def get_data do
    Process.sleep(20)

    "THIS IS DATA!"
  end

  @decorate cacheable(cache: CacheDemo.LocalCache, opts: [ttl: @ttl])
  def get_cached(id) do
    Process.sleep(20)

    "THIS IS DATA!"
  end

  @decorate cacheable(cache: CacheDemo.ReplicatedCache, opts: [ttl: @ttl])
  def get_cached_replicated(id) do
    Process.sleep(20)

    "THIS IS DATA!"
  end
end
